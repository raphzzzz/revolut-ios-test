# Revolut Converter App Task 
Developed using Swift 4.2 and Xcode 10.1

## To run the project 
- Requirements: Xcode 10.1, Swift 4.2
- Clone the repository
- Open it on Xcode
- Run

## What would I do next
- More tests
- Richer UI with the Country Flag on the row

