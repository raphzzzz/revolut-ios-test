//
//  ConverterViewController.swift
//  iOSRevolutTestApp
//
//  Created by Raphael Pedrini Velasqua on 14/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import UIKit

protocol CellViewDelegate {
    func textDidChange(currency: Currency, newValue: String)
    func finishEditing()
}

class ConverterViewController: UIViewController {
    
    @IBOutlet weak var lastUpdated: UILabel!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var tableView: UITableView!

    var viewModel: ConverterViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel = ConverterViewModel(delegate: self)

        setupTableView()

        viewModel.loadCurrencies()
    }
}

extension ConverterViewController : ConverterViewModelDelegate {

    func setupTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: String(describing: CurrencyTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CurrencyTableViewCell.self))
    }

    func loadData() {

        if tableView.visibleCells.count < 1 {
            tableView.reloadData()
        } else {
            if tableView.indexPathForSelectedRow != nil {
                guard var indexPaths = tableView.indexPathsForVisibleRows else {
                    tableView.reloadData()
                    return
                }

                // Exclude first row
                indexPaths.remove(at: 0)

                tableView.reloadRows(at: indexPaths, with: .none)
            } else {
                tableView.reloadData()
            }
        }
    }

    func startLoading() {
        loadingView.isHidden = false
        tableView.isHidden = true
    }

    func stopLoading() {
        loadingView.isHidden = true
        tableView.isHidden = false
        lastUpdated.text = viewModel.getLastUpdatedString()
    }
}

extension ConverterViewController : CellViewDelegate {

    func textDidChange(currency: Currency, newValue: String) {
        viewModel.didChangeCurrencyValue(currency: currency, newValue: newValue)
    }

    func finishEditing() {
        tableView.deselectRow(at: IndexPath(row: 0, section: 0), animated: false)
    }
}

extension ConverterViewController : UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        // Get cell selected, move it to the top, makes TextField first responder
        let cell = tableView.cellForRow(at: indexPath) as! CurrencyTableViewCell

        cell.currencyTextField.isUserInteractionEnabled = true

        let newIndexPath = IndexPath(row: 0, section: 0)

        tableView.beginUpdates()

        tableView.moveRow(at: indexPath, to: newIndexPath)

        viewModel.moveCurrencyToTop(index: indexPath.row)

        tableView.endUpdates()

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            tableView.scrollToRow(at: newIndexPath, at: .top, animated: false)
            cell.currencyTextField.becomeFirstResponder()
        }
    }
}

extension ConverterViewController : UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.currencies.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: CurrencyTableViewCell.self)) as? CurrencyTableViewCell else {
            fatalError("Can't dequeue CurrencyTableViewCell")
        }

        let currency = viewModel.currencies[indexPath.row]

        cell.delegate = self
        cell.selectionStyle = .none

        cell.currency = currency
        cell.currencyName.text = currency.code
        cell.currencyFullName.text = currency.fullName
        cell.currencyTextField.text = "\(viewModel.valueOnCurrentBase(currencyValue: currency.rate))"

        return cell
    }
}
