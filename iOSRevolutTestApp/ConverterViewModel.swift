//
//  ConverterViewModel.swift
//  iOSRevolutTestApp
//
//  Created by Raphael Pedrini Velasqua on 14/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import Foundation

protocol ConverterViewModelDelegate: class {
    func startLoading()
    func stopLoading()
    func loadData()
}

class ConverterViewModel {

    private lazy var model = ConverterService()

    private var haveLoadedOnce = false

    private var timerUpdateInterval = 2.0

    private var baseCurrency = 1.0

    private var currencyUpdateTimer: Timer!

    weak var delegate: ConverterViewModelDelegate!

    var currencies: [Currency] = []

    init(delegate: ConverterViewModelDelegate) {
        self.delegate = delegate
    }

    func loadCurrencies() {
        delegate.startLoading()

        ConverterService().loadCurrencies(completion: self.doneLoadingCurrencies)

        setupTimer()
    }

    func setupTimer() {
        currencyUpdateTimer = Timer.scheduledTimer(withTimeInterval: timerUpdateInterval, repeats: true) { timer in

            ConverterService().loadCurrencies(completion: self.doneLoadingCurrencies)
        }
    }

    func doneLoadingCurrencies(response: ConverterResponse?) {

        if let response = response {
            loadCurrencies(response: response)
        }

        DispatchQueue.main.async {
            self.delegate.stopLoading()
            self.delegate.loadData()
        }
    }

    func getLastUpdatedString() -> String {

        let date = Date()
        let calendar = Calendar.current
        let hour = calendar.component(.hour, from: date)
        let minutes = calendar.component(.minute, from: date)
        let seconds = calendar.component(.second, from: date)

        return "Last Updated: \(hour):\(minutes):\(seconds)"
    }

    func loadCurrencies(response: ConverterResponse) {

        var response = response

        response.currencies.insert(response.base, at: 0)

        if currencies.isEmpty {
            currencies = response.currencies
        } else {
            currencies = reorderResponseRates(newCurrencies: response.currencies)
        }
    }

    func reorderResponseRates(newCurrencies: [Currency]) -> [Currency] {

        var sortedCurrencies: [Currency] = []

        for currency in currencies {
            for newCurrency in newCurrencies {
                if currency.code == newCurrency.code {
                    sortedCurrencies.append(newCurrency)
                }
            }
        }

        return sortedCurrencies
    }

    func didChangeCurrencyValue(currency: Currency, newValue: String) {

        let currencyTypedValue = stringToAcceptableCurrencyRate(str: newValue)

        let newBase = currencyTypedValue/currency.rate

        baseCurrency = newBase

        delegate.loadData()
    }

    func stringToAcceptableCurrencyRate(str: String) -> Double {

        var value = str
        if str.last == "." {
            value.removeLast()
        }

        return Double(value) ?? 0
    }

    func valueOnCurrentBase(currencyValue: Double) -> Double {
        return round(10000*(currencyValue * baseCurrency))/10000
    }

    func moveCurrencyToTop(index: Int) {
        let currency = currencies.remove(at: index)
        currencies.insert(currency, at: 0)
    }
}
