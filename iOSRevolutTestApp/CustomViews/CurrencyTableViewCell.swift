//
//  CurrencyTableViewCell.swift
//  iOSRevolutTestApp
//
//  Created by Raphael Pedrini Velasqua on 14/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import UIKit

class CurrencyTableViewCell: UITableViewCell {

    @IBOutlet weak var currencyName: UILabel!
    @IBOutlet weak var currencyFullName: UILabel!
    @IBOutlet weak var currencyTextField: UITextField!

    var currency: Currency!

    var delegate: CellViewDelegate!

    override func awakeFromNib() {
        super.awakeFromNib()

        currencyTextField.delegate = self
        
        currencyTextField.addDoneButtonToKeyboard(myAction:  #selector(self.currencyTextField.resignFirstResponder))

        currencyTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
    }

    @objc func textFieldDidChange(_ textField: UITextField) {
        delegate.textDidChange(currency: currency, newValue: textField.text ?? "1")
    }
}

extension CurrencyTableViewCell : UITextFieldDelegate {

    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextField.DidEndEditingReason) {
        delegate.finishEditing()
        currencyTextField.isUserInteractionEnabled = false
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        let text = (textField.text ?? "") as NSString
        let newText = text.replacingCharacters(in: range, with: string)
        if let regex = try? NSRegularExpression(pattern: "^[0-9]*((\\.|,)[0-9]{0,5})?$", options: .caseInsensitive) {
            return regex.numberOfMatches(in: newText, options: .reportProgress, range: NSRange(location: 0, length: (newText as NSString).length)) > 0
        }
        return false
    }
}
