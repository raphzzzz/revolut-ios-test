//
//  ConverterResponse.swift
//  iOSRevolutTestApp
//
//  Created by Raphael Pedrini Velasqua on 14/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import Foundation

struct ConverterResponse: Decodable {

    var base: Currency
    var date: String
    var currencies: [Currency]

    enum ConverterKey: String, CodingKey {
        case base
        case date
        case rates
    }

    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: ConverterKey.self)
        base = Currency(code: try container.decode(String.self, forKey: .base), value: 1.0)
        date = try container.decode(String.self, forKey: .date)

        let rates = try container.decode([String: Double].self, forKey: .rates)

        currencies = rates.map { Currency(code: $0.key, value: $0.value) }
    }
}
