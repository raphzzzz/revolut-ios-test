//
//  Currency.swift
//  iOSRevolutTestApp
//
//  Created by Raphael Pedrini Velasqua on 14/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import Foundation

struct Currency {

    var code: String
    var rate: Double
    var fullName: String
    
    init(code: String, value: Double) {
        self.code = code
        self.rate = value
        self.fullName = PListServices().getCurrencyName(code: code)
    }
}
