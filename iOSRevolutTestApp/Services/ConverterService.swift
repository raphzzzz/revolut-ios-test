//
//  ConverterService.swift
//  iOSRevolutTestApp
//
//  Created by Raphael Pedrini Velasqua on 14/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import Foundation

class ConverterService {

    func loadCurrencies(completion: @escaping (ConverterResponse?)->Void) {
        RestAPI().sendAPIRequest(completion: { data in

            if let responseData = data {
                let decoder = JSONDecoder()

                do {
                    let response = try decoder.decode(ConverterResponse.self, from: responseData)

                    completion(response)
                } catch {}
            } else {
                completion(nil)
            }
        })
    }
}
