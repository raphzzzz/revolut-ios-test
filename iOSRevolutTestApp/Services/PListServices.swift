//
//  PListServices.swift
//  iOSRevolutTestApp
//
//  Created by Raphael Pedrini Velasqua on 14/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import Foundation

struct PListServices {

    let currenciesPList: [String : String]?

    init() {
        guard let path = Bundle.main.path(forResource: "CurrenciesFullName", ofType: ".plist"), let dict = NSDictionary(contentsOfFile: path) as? [String : String] else {
            self.currenciesPList = nil
            return
        }

        self.currenciesPList = dict
    }

    public func getCurrencyName(code: String) -> String {

        if let dict = currenciesPList {
            if let currencyName = dict[code] {
                return currencyName
            }
        }

        return ""
    }
}
