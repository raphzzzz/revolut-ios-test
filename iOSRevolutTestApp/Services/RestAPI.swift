//
//  RestAPI.swift
//  iOSRevolutTestApp
//
//  Created by Raphael Pedrini Velasqua on 15/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import Foundation

class RestAPI {

    private static let URL_ENDPOINT = URL(string: "https://revolut.duckdns.org/latest?base=EUR")!

    func sendAPIRequest(completion: @escaping (Data?)->Void) {

        let url = RestAPI.URL_ENDPOINT

        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard let data = data else {
                completion(nil)
                return
            }

            completion(data)
        }

        task.resume()
    }
}
