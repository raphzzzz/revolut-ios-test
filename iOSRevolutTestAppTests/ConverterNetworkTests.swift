//
//  ConverterNetworkTests.swift
//  iOSRevolutTestAppTests
//
//  Created by Raphael Pedrini Velasqua on 15/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import XCTest
import Foundation

@testable import iOSRevolutTestApp

class ConverterNetworkTests: XCTestCase {

    override func setUp() {
    }

    override func tearDown() {
    }

    func testDownloadWebData() {

        let expectation = XCTestExpectation(description: "Reach tech test API")

        let url = URL(string: "https://revolut.duckdns.org/latest?base=EUR")!

        let dataTask = URLSession.shared.dataTask(with: url) { (data, _, _) in

            XCTAssertNotNil(data, "No data was downloaded.")

            expectation.fulfill()

        }

        dataTask.resume()

        wait(for: [expectation], timeout: 10.0)
    }
}
