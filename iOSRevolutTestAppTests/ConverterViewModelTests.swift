//
//  ConverterViewModelTests.swift
//  iOSRevolutTestAppTests
//
//  Created by Raphael Pedrini Velasqua on 15/11/2018.
//  Copyright © 2018 raph. All rights reserved.
//

import XCTest

@testable import iOSRevolutTestApp

class ConverterViewModelTests: XCTestCase {

    let converterViewModel = ConverterViewModel(delegate: ConverterViewControllerMock())

    var converterResponseMock: ConverterResponse?

    override func setUp() {

        let bundle = Bundle(for: ConverterViewModelTests.self)

        if let path = bundle.url(forResource: "ConverterResponse", withExtension: "json") {
            do {
                let data = try Data(contentsOf: path)
                converterResponseMock = try JSONDecoder().decode(ConverterResponse.self, from: data)
            } catch {
                XCTFail()
            }
        }
    }

    override func tearDown() {
    }

    func testInit_initViewModel() {
        XCTAssertNotNil(converterViewModel)
    }

    func testInit_InitConverterResponseFromMock() {
        XCTAssertNotNil(converterResponseMock)
    }

    func testInit_InitCurrencyFromResponse() {
        converterViewModel.currencies = converterResponseMock?.currencies ?? []
        XCTAssertFalse(converterViewModel.currencies.isEmpty)
    }

    func testInit_CreateBaseCurrencyFromResponse() {
        XCTAssertNotNil(converterResponseMock?.base)
    }

    func testInit_CreateSortedCurrencyFromResponse() {
        XCTAssertNotNil(converterViewModel.reorderResponseRates(newCurrencies: (converterResponseMock?.currencies)!))
    }

    func testInit_ChangeStringToAcceptableRate() {
        XCTAssertNotNil(converterViewModel.stringToAcceptableCurrencyRate(str: "1.2"))
    }

    func testInit_ChangeCurrencyValue() {

        let currencyTypedValue = converterViewModel.stringToAcceptableCurrencyRate(str: "1.0")

        let newBase = currencyTypedValue/1.7

        XCTAssertNotNil(newBase)
    }

    func testInit_valueOnCurrentBase() {
        let someDoubleValue = converterViewModel.valueOnCurrentBase(currencyValue: 1.5)
        XCTAssertNotNil(someDoubleValue)
    }
}

class ConverterViewControllerMock: ConverterViewModelDelegate {

    func startLoading() {}

    func stopLoading() {}

    func loadData() {}
}
